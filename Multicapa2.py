# Import
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from random import shuffle
import sys 

FILE = sys.argv[1]
EPOCHS = int(sys.argv[2])
VAL = int(sys.argv[3])
BATCH_SIZE = int(sys.argv[4])
RESULT = int(sys.argv[5])

with open(FILE) as ip:
    lines=ip.readlines()
    header = lines.pop(0)
    shuffle(lines)
    lines.insert(0, header)

with open('shuffled.csv','w') as out:
    out.writelines(lines)

# Import data
data = pd.read_csv('shuffled.csv')

# Dimensions of dataset
n = data.shape[0]
p = data.shape[1]

# Make data a np.array
data = data.values


def split_train_test(data):
    # Training and test data
    train_start = 0
    train_end = int(np.floor(0.8*n))
    test_start = train_end + 1
    test_end = n
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    return {'data_train' :data_train, 'data_test' :data_test}

def scale(values):
    # Scale data
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaler.fit(values['data_train'])
    data_train = scaler.transform(values['data_train'])
    data_test = scaler.transform(values['data_test'])
    return {'data_train' :data_train, 'data_test' :data_test}

def build(values, var):
    data_train = values['data_train']
    data_test = values['data_test']

    # Build X and y
    X_train = data_train[:, 6:]
    X_test = data_test[:, 6:]
    y_train = data_train[:, var]
    y_test = data_test[:, var]

    return {'X_train' :X_train, 'X_test' :X_test, 'y_train' :y_train, 'y_test' :y_test}

def model(X, n_nodes):

    # Initializers
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    layers = [None]*len(n_nodes)
    layers[0] = X

    # Hidden weights
    for i in range(1,len(n_nodes)):
        W_hidden = tf.Variable(weight_initializer([n_nodes[i-1], n_nodes[i]]))
        bias_hidden = tf.Variable(bias_initializer([n_nodes[i]]))
        layers[i] = tf.nn.relu(tf.add(tf.matmul(layers[i-1], W_hidden), bias_hidden))
        
    # Output weights
    W_out = tf.Variable(weight_initializer([n_nodes[-1], 1]))
    bias_out = tf.Variable(bias_initializer([1]))
    out = tf.transpose(tf.add(tf.matmul(layers[-1], W_out), bias_out))

    return out

def train(data, epochs, batch_size, val):

    values = scale(split_train_test(data))
    #values = split_train_test(data)
    Xy = build(values, val)
    X_train = Xy['X_train']
    X_test = Xy['X_test']
    y_train = Xy['y_train']
    y_test = Xy['y_test']

    # Number of stocks in training data
    n_stocks = Xy['X_train'].shape[1]

    # Neurons
    n_nodes = [n_stocks, 32, 16, 8]

    # Session
    net = tf.compat.v1.InteractiveSession()

    # Placeholder
    X = tf.placeholder(dtype=tf.float32, shape=[None, n_stocks])
    Y = tf.placeholder(dtype=tf.float32, shape=[None])

    out = model(X,n_nodes)

    # Cost function
    mse = tf.reduce_mean(tf.squared_difference(out, Y))

    # Optimizer
    opt = tf.train.AdamOptimizer().minimize(mse)

    # Init
    net.run(tf.global_variables_initializer())

    # Run
    for e in range(epochs):

        # Shuffle training data
        shuffle_indices = np.random.permutation(np.arange(len(y_train)))
        X_train = X_train[shuffle_indices]
        y_train = y_train[shuffle_indices]

        # Minibatch training
        for i in range(0, len(y_train)//batch_size):
            start = i 
            batch_x = X_train[start:start + batch_size]
            batch_y = y_train[start:start + batch_size]
            # Run optimizer with batch
            net.run(opt, feed_dict={X: batch_x, Y: batch_y})

            # Prediction
            pred = net.run(out, feed_dict={X: X_test})
            
    saver = tf.train.Saver()
    saver.save(net, "modelo_entrenado.ckpt")
    return(X, X_test, out)
    
X, X_test, out = train(data,EPOCHS,BATCH_SIZE,VAL)

with tf.Session() as net2:
    saver = tf.train.Saver()
    saver.restore(net2, "modelo_entrenado.ckpt")
    prediction = net2.run(out, feed_dict={X: X_test})