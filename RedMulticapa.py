# Import
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from random import shuffle
import sys 

FILE = sys.argv[1]
EPOCHS = int(sys.argv[2])
VAL = int(sys.argv[3])
BATCH_SIZE = int(sys.argv[4])
RESULT = int(sys.argv[5])

with open(FILE) as ip:
    lines=ip.readlines()
    header = lines.pop(0)
    shuffle(lines)
    lines.insert(0, header)

with open('shuffled.csv','w') as out:
    out.writelines(lines)

# Import data
data = pd.read_csv('shuffled.csv')
plt.style.use('ggplot')
#plt.plot(data['beta'])
#plt.show()

# Dimensions of dataset
n = data.shape[0]
p = data.shape[1]

# Make data a np.array
data = data.values


def split_train_test(data):
    # Training and test data
    train_start = 0
    train_end = int(np.floor(0.999*n))
    test_start = train_end + 1
    test_end = n
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    return {'data_train' :data_train, 'data_test' :data_test}

def scale(values):
    # Scale data
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaler.fit(values['data_train'])
    data_train = scaler.transform(values['data_train'])
    data_test = scaler.transform(values['data_test'])
    return {'data_train' :data_train, 'data_test' :data_test}

def build(values, var):
    data_train = values['data_train']
    data_test = values['data_test']

    # Build X and y
    X_train = data_train[:, 7:]
    X_test = data_test[:, 7:]
    y_train = data_train[:, var]
    y_test = data_test[:, var]

    return {'X_train' :X_train, 'X_test' :X_test, 'y_train' :y_train, 'y_test' :y_test}

def model(X, n_nodes):

    # Initializers
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    layers = [None]*len(n_nodes)
    layers[0] = X

    # Hidden weights
    for i in range(1,len(n_nodes)):
        W_hidden = tf.Variable(weight_initializer([n_nodes[i-1], n_nodes[i]]))
        bias_hidden = tf.Variable(bias_initializer([n_nodes[i]]))
        layers[i] = tf.nn.relu(tf.add(tf.matmul(layers[i-1], W_hidden), bias_hidden))
        
    # Output weights
    W_out = tf.Variable(weight_initializer([n_nodes[-1], 1]))
    bias_out = tf.Variable(bias_initializer([1]))
    out = tf.transpose(tf.add(tf.matmul(layers[-1], W_out), bias_out))

    return out

def train(data, epochs, batch_size, val):

    values = scale(split_train_test(data))
    #values = split_train_test(data)
    Xy = build(values, val)
    X_train = Xy['X_train']
    X_test = Xy['X_test']
    y_train = Xy['y_train']
    y_test = Xy['y_test']


    # Number of stocks in training data
    n_stocks = Xy['X_train'].shape[1]
    #print(n_stocks)

    # Neurons
    n_nodes = [n_stocks, 32, 16, 8]

    # Session0
    net = tf.compat.v1.InteractiveSession()

    # Placeholder
    X = tf.placeholder(dtype=tf.float32, shape=[None, n_stocks])
    Y = tf.placeholder(dtype=tf.float32, shape=[None])

    out = model(X,n_nodes)

    # Cost function
    mse = tf.reduce_mean(tf.squared_difference(out, Y))

    # Optimizer
    opt = tf.train.AdamOptimizer().minimize(mse)

    # Init
    net.run(tf.global_variables_initializer())

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()

    # Setup plot
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    if RESULT:
        line1, = ax1.plot(y_test)
        line2, = ax1.plot(y_test * 0.5)

    # Fit neural net
    mse_train = []
    mse_test = []

    # Run
    #name = "b0.pdf"
    #fig.savefig(name, bbox_inches='tight')
    for e in range(epochs):

        # Shuffle training data
        shuffle_indices = np.random.permutation(np.arange(len(y_train)))
        X_train = X_train[shuffle_indices]
        y_train = y_train[shuffle_indices]

        # Minibatch training
        for i in range(0, len(y_train)//batch_size):
            start = i 
            batch_x = X_train[start:start + batch_size]
            batch_y = y_train[start:start + batch_size]
            # Run optimizer with batch
            net.run(opt, feed_dict={X: batch_x, Y: batch_y})

            # Show progress
            # MSE train and test
            mse_train.append(net.run(mse, feed_dict={X: X_train, Y: y_train}))
            mse_test.append(net.run(mse, feed_dict={X: X_test, Y: y_test}))
            print('MSE Train: ', mse_train[-1])
            print('MSE Test: ', mse_test[-1])
            # Prediction
            pred = net.run(out, feed_dict={X: X_test})
            plt.pause(0.01)
            if RESULT:
                line2.set_ydata(pred)
                plt.title('Epoch ' + str(e) + ', Batch ' + str(i))
            else:
                #if (i == 0 or i == 8):
                #    name = "kp_nsca_nsh" + str(e) + "_" +str(i) + ".pdf"
                #    fig.savefig(name, bbox_inches='tight')  

                plt.title('Error del entrenamiento vs el error de las pruebas')
                plt.xlabel('Iteraciones', fontsize=18)
                plt.ylabel("Magnitud del error", fontsize=18)
                plt.plot(mse_train, color = '#E74C3C', linewidth=0.1)
                plt.plot(mse_test, color = '#3498DB',linewidth=0.1)
            
        #name = "errornshsc.pdf"
        #fig.savefig(name, bbox_inches='tight')
    print(pred)
    saver.save(net, "modelo_entrenado.ckpt")
    return(X, X_test, out)
    
X, X_test, out = train(data,EPOCHS,BATCH_SIZE,VAL)

with tf.Session() as net:
    saver = tf.train.Saver()
    saver.restore(net, "modelo_entrenado.ckpt")
    prediction = net.run(out, feed_dict={X: X_test})
    print(prediction)
