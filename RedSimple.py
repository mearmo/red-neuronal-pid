import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
import sys 

CANTIDAD = int(sys.argv[1])
EPOCHS = int(sys.argv[2])

xs = np.array([0.0], dtype=float)
ys = np.array([0.0], dtype=float)
for i in range(CANTIDAD):
    xs = np.append(xs,i)
    y = 10.0*i + 1.0
    #y = 2*i**2 + 2
    #y = i**3 + i + 3
    ys = np.append(ys,y)

xs = np.delete(xs,0)
ys = np.delete(ys,0)

model = tf.keras.Sequential([keras.layers.Dense(units=1, input_shape=[1])])
model.compile(optimizer='sgd', loss='mean_squared_error', metrics ='accuracy')
history = model.fit(xs, ys, epochs=EPOCHS)
print(model.predict([13]))
print("Variables de peso y sesgo: {}".format(model.get_weights()))

titulo = 'Entrenamiento de ' + str(CANTIDAD) + ' datos en ' + str(EPOCHS) + ' iteraciones en expresión cuadrática'

plt.style.use('ggplot')
plt.title(titulo)
plt.xlabel('Iteraciones', fontsize=18)
plt.ylabel("Magnitud del error", fontsize=18)
plt.plot(history.history['loss'], color='#8A0829')
plt.tight_layout()
plt.show()

